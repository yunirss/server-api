FROM openjdk:17.0.1-jdk-slim
ARG JAR_FILE=target/server-api-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
EXPOSE 8080
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]