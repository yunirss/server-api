**Server-API**

`Описание проекта`

> Server API - web приложение по добавлению логов. С помощью REST API реализованы HTTP запросы для сохранения данных в СУБД PostgreSQL и одновременно в текстовый файл "log.txt". Сохранение данных производится в формате JSON.

`Запуск приложения`

> Необходима База Данных PostgreSQL. Для начала нужно клонировать репозиторий, запустить проект.
Затем нужно запустить сборку проекта через Maven. Выполнить такие команды как "clean" и "install". Также мною был разработан Dockerfile в корневой папке приложения. 
Для запуска приложения на контейнерах Докера нужно в файле application.properties поменять строку "localhost" на "host.docker.internal", после этого использовать команды в Терминале "docker build ." -> "docker images" -> "docker run -p 8080:8080 (здесь подставить ID образа)" и соответственно отправить запросы на добавление лога в Postman. Приложение запущено! Для просмотра документации проекта с помощью Swagger можно перейти по адресу http://localhost:8080/swagger-ui/index.html , после запуска приложения.

`Требования к SOFTWARE`
- MacOs
- Windows
- Linux

`Среда разработки`
- IntelliJ IDEA Ultimate

`Стек технологий`
- Java 17
- Apache Maven version 4.0.0
- Spring Boot 3.1.0
- Spring WebMVC
- Spring Data JPA
- Lombok
- PostgreSQL
- Docker

`Требования и задачи по проекту описаны в файле task.md.`

**REST запросы :**

`GET запрос:`
- Healthcheck - /core-api/healthcheck

`POST запрос:`
- Save log - /core-api/logs

`Разработчик`
- Суюндуков Юнир Ильфатович
- yunir14@yandex.ru
- t.me/yunirss
