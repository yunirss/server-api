package ru.suyundukov.serverapi.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.suyundukov.serverapi.dto.LogDto;
import ru.suyundukov.serverapi.entity.Log;

@Mapper(componentModel = "spring")
public interface LogMapper {
    LogMapper INSTANCE = Mappers.getMapper(LogMapper.class);

    Log toEntity(LogDto dto);
}
