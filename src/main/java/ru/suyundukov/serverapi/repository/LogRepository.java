package ru.suyundukov.serverapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.suyundukov.serverapi.entity.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {
}
