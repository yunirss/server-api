package ru.suyundukov.serverapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.stereotype.Service;
import ru.suyundukov.serverapi.entity.Log;
import ru.suyundukov.serverapi.repository.LogRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

@Service
public class LogService {
    private final LogRepository logRepository;

    public LogService(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public void save(Log log) {
        log.setTime(LocalDateTime.now());
        logRepository.save(log);
    }

    public void saveLogToFile(Log log) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String logJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(log);
        Files.write(Paths.get("logs.txt"), logJson.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }
}
