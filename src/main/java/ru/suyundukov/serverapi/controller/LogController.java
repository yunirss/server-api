package ru.suyundukov.serverapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.serverapi.dto.LogDto;
import ru.suyundukov.serverapi.entity.Log;
import ru.suyundukov.serverapi.mapper.LogMapper;
import ru.suyundukov.serverapi.service.LogService;

import java.io.IOException;

@RestController
@RequestMapping("/core-api")
@Tag(name = "Контроллер для логов", description = "Позволяет работать с логами")
public class LogController {

    private final LogService service;

    public LogController(LogService service) {
        this.service = service;
    }

    @Operation(
            summary = "Добавление лога",
            description = "Позволяет добавить лог"
    )
    @PostMapping(value = "/logs", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Log> addLogs(@RequestBody LogDto logDto) throws IOException {
        Log log = LogMapper.INSTANCE.toEntity(logDto);
        service.save(log);
        service.saveLogToFile(log);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(
            summary = "Проверка сервера",
            description = "Позволяет проверить сервер на работоспособность"
    )
    @GetMapping("/healthcheck")
    public ResponseEntity<Void> healthCheck() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
