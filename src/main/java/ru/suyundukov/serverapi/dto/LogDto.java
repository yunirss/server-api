package ru.suyundukov.serverapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Сущность лога")
public class LogDto {
    @Schema(description = "Сообщение")
    private String message;
    @Schema(description = "Тип")
    private String type;
    @Schema(description = "Уровень")
    private String level;
}
