**Server-API**

Приложение реализует один метод /logs позволяющий отправить сообщение в виде JSON, записывает его в БД и в этом же виде в виде строки вjson формате в файл

Должна быть инструкция как на локали запустить 

- Описание: Сервер API (JSON HTTP API)

- Средства разработки: Java

- Framework: Spring Boot

- context-path: core-api

- База данных: pg 

- Протокол: HTTP, порт 80

- Функционал (запросы):

    - Добавление лога с фронта

        - Передаем JSON с атрибутами:

            - message - text

            - type - text

            - level - text

            - time - timestamp

        - Сообщение нужно записать в БД в том же формате и записать в файл лога

        - Ответ сервера - 200 ОК

 

Обязательные требования:

- RESTful

- Обработка ошибок

- Есть endpoint для healthcheck-а

 

Необязательные требования (желательно):

- Документирование кода в формате OpenAPI (swagger)

- Тесты.

 

1.       Приложение должно обеспечить стандартными средствами логирование операций и ошибок (пишет в файл)

2.       Реализует один метод /logs позволяющий вывести на экран содержимое лога по запросу (например: http://loaclhost:8082/logs)

3.       Записывать логи в БД – дата, метод, операция, ошибка / сообщение, ip адрес клиента
